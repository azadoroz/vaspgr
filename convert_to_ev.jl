using Printf;
int1cm=readlines("exp_abs_1_cm");
int1ev=Array{Float64, 2}(undef, 2, length(int1cm));
for i=1:length(int1cm)
	tmp_st=split(int1cm[i]);
	enCm=parse(Float32, tmp_st[1]);
	int1ev[1,i]=enCm/8.0655;
	int1ev[2,i]=parse(Float32, tmp_st[2]);
end
open("int1.txt", "w") do io
	for i=1:length(int1cm)
		out=@sprintf("%3.8f\t%3.8f\n", int1ev[1,i], int1ev[2,i]);
		write(io, out);
	end;
end;

int2cm=readlines("exp_abs_2_cm");
int2ev=Array{Float64, 2}(undef, 2, length(int2cm));
for i=1:length(int2cm)
	tmp_st=split(int2cm[i]);
	enCm=parse(Float32, tmp_st[1]);
	int2ev[1,i]=enCm/8.0655;
	int2ev[2,i]=parse(Float32, tmp_st[2]);
end
open("int2.txt", "w") do io
	for i=1:length(int2cm)
		out=@sprintf("%3.8f\t%3.8f\n", int2ev[1,i], int2ev[2,i]);
		write(io, out);
	end;
end;

int3cm=readlines("exp_abs_3_cm");
int3ev=Array{Float64, 2}(undef, 2, length(int3cm));
for i=1:length(int3cm)
	tmp_st=split(int3cm[i]);
	enCm=parse(Float32, tmp_st[1]);
	int3ev[1,i]=enCm/8.0655;
	int3ev[2,i]=parse(Float32, tmp_st[2]);
end
open("int3.txt", "w") do io
	for i=1:length(int3cm)
		out=@sprintf("%3.8f\t%3.8f\n", int3ev[1,i], int3ev[2,i]);
		write(io, out);
	end;
end;