using Printf;
include("makeN.jl");
include("makeDOS.jl");

function getThisDirName()
	return replace(last(split(read(`pwd`, String),'/')), "\n"=>"");
end
thisDirName=getThisDirName();
U=@sprintf("%2.2f", 2+parse(Float32, thisDirName));
#Prefics is used to specify the title. 
prefix=string("U = ", U, ", J = ");
outputFileNameDOS=string("dos_", thisDirName, ".png");
outputFileNameN=string("n_",thisDirName, ".png");
outputFileNameEps=string("eps_",thisDirName, ".png");

run(`gnuplot -e "output_filename='$outputFileNameN'; graph_title='$prefix$thisDirName'" -persist plotN.gpt`);
run(`gnuplot -e "output_filename='$outputFileNameEps'; graph_title='$prefix$thisDirName'" -persist plotEps.gpt`);
run(`gnuplot -e "output_filename='$outputFileNameDOS'; graph_title='$prefix$thisDirName'" -persist plotDOS.gpt`);


run(`convert $outputFileNameDOS $outputFileNameEps +append $thisDirName.png`);
run(`rm $outputFileNameDOS $outputFileNameN $outputFileNameEps`);