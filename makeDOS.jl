#=
println(PROGRAM_FILE); 
for x in ARGS; 
println(x); 
end

println(countlines(PROGRAM_FILE));
a="sdgfg";
b="fdgas";

println(string(a, b));
println(length(a));

txtread=open(f->read(f, String), "doc");
println(txtread);
txtsplit=split(txtread);
println(txtsplit);

numread = zeros(0);
for x in txtsplit 
append!(numread, parse(Float32, x));
end
println(numread);


numread = Array{Int, 1}(undef,0);
append!(numread, 22);
println(numread);

aa=5 +
5;
println(aa);


#JUST A QUICK TEST
open("myfile.txt", "w") do io
     for i=1:numberOfLinesPerAtom
	sum_up=0;
	sum_down=0;

	for atomNumber=1:numberOfAtoms
		sum_up=sum_up+p_up[atomNumber, i]+s_up[atomNumber, i]+d_up[atomNumber, i];
		sum_down=sum_down+p_down[atomNumber, i]+s_down[atomNumber, i]+d_down[atomNumber, i];
	end
		write(io,string(energy[i], '\t', totalDosUp[i], '\t', sum_up,'\n'));
		
	if (false&&abs(totalDosDown[i]-sum_down)>1e-4)
		println("DOWN\t", energy[i], '\t', totalDosDown[i], '\t', sum_down);
	end
     end
end;


open("myfile.txt", "w") do pony
write(pony, "Hello world!");
end;
=#

using Printf;

doscar=readlines("DOSCAR");
#println(doscar[1]);

numberOfAtoms=parse(Int, (split(doscar[1]))[2]);
#println(numberOfAtoms);
fermiEnergy=parse(Float64, (split(doscar[6]))[3]);
#println(fermiEnergy);

numberOfLinesPerAtom=(length(doscar) - 5 - numberOfAtoms - 1) / (numberOfAtoms + 1); #i. e. NEDOS
numberOfLinesPerAtom=convert(Int, numberOfLinesPerAtom);
# - 5 is for first 5 lines
# - numberOfAtoms - 1 is for lines before each block representing 
# E(max), E(min), (the energy range in which the DOS is given), NEDOS,  E(fermi), 1.0000
# / (numberOfAtoms + 1) is for numberOfAtoms atom-wise DOS block and one total DOS block
#println(numberOfLinesPerAtom);

#RUNNING TOTAL DOS
#ASSUMING NON-COLLINEAR SPIN-POLARIZED CALCULATIONS
energy=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalDosUp=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalDosDown=Array{Float64, 1}(undef, numberOfLinesPerAtom);
integratedDosUp=Array{Float64, 1}(undef, numberOfLinesPerAtom);
integratedDosDown=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalDos=Array{Float64, 1}(undef, numberOfLinesPerAtom);
integratedDos=Array{Float64, 1}(undef, numberOfLinesPerAtom);

for i=1:numberOfLinesPerAtom
	lnum=6+i; #line number
	stringNumArray=split(doscar[lnum]);
	energy[i]=parse(Float64, stringNumArray[1]) - fermiEnergy;
	totalDosUp[i]=parse(Float64, stringNumArray[2]);
	totalDosDown[i]=parse(Float64, stringNumArray[3]);
	integratedDosUp[i]=parse(Float64, stringNumArray[4]);
	integratedDosDown[i]=parse(Float64, stringNumArray[5]);
	totalDos[i]=totalDosUp[i]+totalDosDown[i];
	integratedDos[i]=integratedDosUp[i]+integratedDosDown[i];
end

#ATOM-WISE
s_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
s_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);

p_y_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
p_y_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
p_z_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
p_z_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
p_x_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
p_x_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);

d_xy_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_xy_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_yz_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_yz_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_z2_min_r2_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_z2_min_r2_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_xz_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_xz_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_x2_min_y2_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_x2_min_y2_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);

p_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
p_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_up=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);
d_down=Array{Float64, 2}(undef, numberOfAtoms, numberOfLinesPerAtom);


for atomNumber=1:numberOfAtoms
	for i=1:numberOfLinesPerAtom
		lnum= 5 + (numberOfLinesPerAtom + 1) * atomNumber + 1 + i;
		#5 for first 5 lines, (numberOfLinesPerAtom + 1) * atomNumber for how many blocks we should skip, +1 for the first line in each block
		stringNumArray=split(doscar[lnum]);

		s_up[atomNumber, i]=parse(Float64, stringNumArray[2]);
		s_down[atomNumber, i]=parse(Float64, stringNumArray[3]);

		p_y_up[atomNumber, i]=parse(Float64, stringNumArray[4]);
		p_y_down[atomNumber, i]=parse(Float64, stringNumArray[5]);
		p_z_up[atomNumber, i]=parse(Float64, stringNumArray[6]);
		p_z_down[atomNumber, i]=parse(Float64, stringNumArray[7]);
		p_x_up[atomNumber, i]=parse(Float64, stringNumArray[8]);
		p_x_down[atomNumber, i]=parse(Float64, stringNumArray[9]);

		d_xy_up[atomNumber, i]=parse(Float64, stringNumArray[10]);
		d_xy_down[atomNumber, i]=parse(Float64, stringNumArray[11]);
		d_yz_up[atomNumber, i]=parse(Float64, stringNumArray[12]);
		d_yz_down[atomNumber, i]=parse(Float64, stringNumArray[13]);
		d_z2_min_r2_up[atomNumber, i]=parse(Float64, stringNumArray[14]);
		d_z2_min_r2_down[atomNumber, i]=parse(Float64, stringNumArray[15]);
		d_xz_up[atomNumber, i]=parse(Float64, stringNumArray[16]);
		d_xz_down[atomNumber, i]=parse(Float64, stringNumArray[17]);
		d_x2_min_y2_up[atomNumber, i]=parse(Float64, stringNumArray[18]);
		d_x2_min_y2_down[atomNumber, i]=parse(Float64, stringNumArray[19]);

		p_up[atomNumber, i]=	p_y_up[atomNumber, i]+
					p_z_up[atomNumber, i]+
					p_x_up[atomNumber, i];

		p_down[atomNumber, i]=	p_y_down[atomNumber, i]+
					p_z_down[atomNumber, i]+
					p_x_down[atomNumber, i];

		d_up[atomNumber, i]=	d_xy_up[atomNumber, i]+
					d_yz_up[atomNumber, i]+
					d_z2_min_r2_up[atomNumber, i]+
					d_xz_up[atomNumber, i]+
					d_x2_min_y2_up[atomNumber, i];

		d_down[atomNumber, i]=	d_xy_down[atomNumber, i]+
					d_yz_down[atomNumber, i]+
					d_z2_min_r2_down[atomNumber, i]+
					d_xz_down[atomNumber, i]+
					d_x2_min_y2_down[atomNumber, i];
	end
end



#NOW PRECISE TASK GOES, WITH 6 Br AND 2 Cr ATOMS 

totalBrSUp=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalBrPUp=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalBrDUp=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalCrSUp=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalCrPUp=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalCrDUp=Array{Float64, 1}(undef, numberOfLinesPerAtom);

totalBrSDown=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalBrPDown=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalBrDDown=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalCrSDown=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalCrPDown=Array{Float64, 1}(undef, numberOfLinesPerAtom);
totalCrDDown=Array{Float64, 1}(undef, numberOfLinesPerAtom);

for i=1:numberOfLinesPerAtom
	totalBrSUp[i]=	s_up[1,i]+
			s_up[2,i]+
			s_up[3,i]+
			s_up[4,i]+
			s_up[5,i]+
			s_up[6,i];

	totalBrPUp[i]=	p_up[1,i]+
			p_up[2,i]+
			p_up[3,i]+
			p_up[4,i]+
			p_up[5,i]+
			p_up[6,i];

	totalBrDUp[i]=	d_up[1,i]+
			d_up[2,i]+
			d_up[3,i]+
			d_up[4,i]+
			d_up[5,i]+
			d_up[6,i];

	totalCrSUp[i]=	s_up[7,i]+
			s_up[8,i];

	totalCrPUp[i]=	p_up[7,i]+
			p_up[8,i];

	totalCrDUp[i]=	d_up[7,i]+
			d_up[8,i];



	totalBrSDown[i]=	s_down[1,i]+
				s_down[2,i]+
				s_down[3,i]+
				s_down[4,i]+
				s_down[5,i]+
				s_down[6,i];

	totalBrPDown[i]=	p_down[1,i]+
				p_down[2,i]+
				p_down[3,i]+
				p_down[4,i]+
				p_down[5,i]+
				p_down[6,i];

	totalBrDDown[i]=	d_down[1,i]+
				d_down[2,i]+
				d_down[3,i]+
				d_down[4,i]+
				d_down[5,i]+
				d_down[6,i];

	totalCrSDown[i]=	s_down[7,i]+
				s_down[8,i];

	totalCrPDown[i]=	p_down[7,i]+
				p_down[8,i];

	totalCrDDown[i]=	d_down[7,i]+
				d_down[8,i];
end

open("BrDOS.txt", "w") do brOutput
	for i=1:numberOfLinesPerAtom
		out=@sprintf("%3.8f\t%3.8f\t%3.8f\t%3.8f\t%3.8f\t%3.8f\t%3.8f\t\n", 
			energy[i], 
			totalBrSUp[i], -totalBrSDown[i], 
			totalBrPUp[i], -totalBrPDown[i], 
			totalBrDUp[i], -totalBrDDown[i]);
		write(brOutput, out);
	end
end


open("CrDOS.txt", "w") do crOutput
	for i=1:numberOfLinesPerAtom
		out=@sprintf("%3.8f\t%3.8f\t%3.8f\t%3.8f\t%3.8f\t%3.8f\t%3.8f\n", 
			energy[i], 
			totalCrSUp[i], -totalCrSDown[i], 
			totalCrPUp[i], -totalCrPDown[i], 
			totalCrDUp[i], -totalCrDDown[i]);
		write(crOutput, out);
	end
end

open("totalDOS.txt", "w") do dosOutput
	for i=1:numberOfLinesPerAtom
		out=@sprintf("%3.8f\t%3.8f\t%3.8f\n", 
			energy[i], 
			totalDosUp[i], -totalDosDown[i]);
		write(dosOutput, out);
	end
end