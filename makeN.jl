using Printf;

function getNedos(outcar)
	textToFind="NEDOS";
	nedosLineNumber=findfirst(x ->
		occursin(textToFind, x), 
		outcar);
	splittedLine=split(outcar[nedosLineNumber]);
	return parse(Int, splittedLine[findfirst(w -> w==textToFind, splittedLine) + 2]);
	#findfirst(w -> w==textToFind, splittedLine) is the number of the element equal to NEDOS
	#findfirst(w -> w==textToFind, splittedLine)+2 is because line looks like 
	#"bla bla bla NEDOS = 12345 bla bla", so we should take second element after NEDOS
end;
#------------------------------------------------------------------------

outcar=readlines("OUTCAR");
lineWhereImaginaryPartBegins=findfirst(x ->
	occursin("IMAGINARY DIELECTRIC FUNCTION (independent particle, no local field effects) density-density", x), 
	outcar);

lineWhereRealPartBegins=findfirst(x ->
	occursin("REAL DIELECTRIC FUNCTION (independent particle, no local field effects) density-density", x), 
	outcar);
NEDOS=getNedos(outcar);



#println(lineWhereImaginaryPartBegins);
#println(lineWhereRealPartBegins);
#println(NEDOS);
#println(length(outcar));

#READING IMAGINARY PART OF DIELECTRIC FUNCTION

photonEnergy  = Array{Float64, 1}(undef, NEDOS);
imagXEpsilon  = Array{Float64, 1}(undef, NEDOS);
imagYEpsilon  = Array{Float64, 1}(undef, NEDOS);
imagZEpsilon  = Array{Float64, 1}(undef, NEDOS);
imagXYEpsilon = Array{Float64, 1}(undef, NEDOS);
imagYZEpsilon = Array{Float64, 1}(undef, NEDOS);
imagZXEpsilon = Array{Float64, 1}(undef, NEDOS);

for i=1:NEDOS
	lnum=lineWhereImaginaryPartBegins + 2 + i;
	stringNumArray=split(outcar[lnum]);
	
	photonEnergy[i]=	parse(Float64, stringNumArray[1]);
	imagXEpsilon[i]=	parse(Float64, stringNumArray[2]);
	imagYEpsilon[i]=	parse(Float64, stringNumArray[3]);
	imagZEpsilon[i]=	parse(Float64, stringNumArray[4]);
	imagXYEpsilon[i]=	parse(Float64, stringNumArray[5]);
	imagYZEpsilon[i]=	parse(Float64, stringNumArray[6]);
	imagZXEpsilon[i]=	parse(Float64, stringNumArray[7]);
end;

#READING REAL PART OF DIELECTRIC FUNCTION

realXEpsilon=Array{Float64, 1}(undef, NEDOS);
realYEpsilon=Array{Float64, 1}(undef, NEDOS);
realZEpsilon=Array{Float64, 1}(undef, NEDOS);
realXYEpsilon=Array{Float64, 1}(undef, NEDOS);
realYZEpsilon=Array{Float64, 1}(undef, NEDOS);
realZXEpsilon=Array{Float64, 1}(undef, NEDOS);

for i=1:NEDOS
	lnum=lineWhereRealPartBegins + 2 + i;
	stringNumArray=split(outcar[lnum]);
	
	realXEpsilon[i]=	parse(Float64, stringNumArray[2]);
	realYEpsilon[i]=	parse(Float64, stringNumArray[3]);
	realZEpsilon[i]=	parse(Float64, stringNumArray[4]);
	realXYEpsilon[i]=	parse(Float64, stringNumArray[5]);
	realYZEpsilon[i]=	parse(Float64, stringNumArray[6]);
	realZXEpsilon[i]=	parse(Float64, stringNumArray[7]);
end;

#TURNING INTO COMPLEX NUMBERS

XEpsilon  = Array{Complex{Float64}, 1}(undef, NEDOS);
YEpsilon  = Array{Complex{Float64}, 1}(undef, NEDOS);
ZEpsilon  = Array{Complex{Float64}, 1}(undef, NEDOS);
XYEpsilon = Array{Complex{Float64}, 1}(undef, NEDOS);
YZEpsilon = Array{Complex{Float64}, 1}(undef, NEDOS);
ZXEpsilon = Array{Complex{Float64}, 1}(undef, NEDOS);

for i=1:NEDOS
	XEpsilon[i]  =	realXEpsilon[i]  + imagXEpsilon[i]*im;
	YEpsilon[i]  =	realYEpsilon[i]  + imagYEpsilon[i]*im;
	ZEpsilon[i]  =	realZEpsilon[i]  + imagZEpsilon[i]*im;
	XYEpsilon[i] =	realXYEpsilon[i] + imagXYEpsilon[i]*im;
	YZEpsilon[i] =	realYZEpsilon[i] + imagYZEpsilon[i]*im;
	ZXEpsilon[i] =	realZXEpsilon[i] + imagZXEpsilon[i]*im;
end;

#CALCULATING REFRACTION COEFFICIENT

XN  = Array{Complex{Float64}, 1}(undef, NEDOS);
YN  = Array{Complex{Float64}, 1}(undef, NEDOS);
ZN  = Array{Complex{Float64}, 1}(undef, NEDOS);
XYN = Array{Complex{Float64}, 1}(undef, NEDOS);
YZN = Array{Complex{Float64}, 1}(undef, NEDOS);
ZXN = Array{Complex{Float64}, 1}(undef, NEDOS);

for i=1:NEDOS
	XN[i]  =	sqrt(XEpsilon[i]);
	YN[i]  =	sqrt(YEpsilon[i]);
	ZN[i]  =	sqrt(ZEpsilon[i]);
	XYN[i] =	sqrt(XYEpsilon[i]);
	YZN[i] =	sqrt(YZEpsilon[i]);
	ZXN[i] =	sqrt(ZXEpsilon[i]);
end;

#ASSUMING THE TENSOR TO BE DIAGONAL, EXPORTING REAL AND IMAGINARY PARTS OF X COMPONENT (XN) OF THE REFRACTION COEFFICIENT

open("N.txt", "w") do nOutput
	for i=1:NEDOS
		out=@sprintf("%3.8f\t%3.8f\t%3.8f\n", photonEnergy[i], real(XN[i]), imag(XN[i]));
		write(nOutput, out);
	end;
end;
open("Eps.txt", "w") do epsOutput
	for i=1:NEDOS
		out=@sprintf("%3.8f\t%3.8f\t%3.8f\n", photonEnergy[i], real(XEpsilon[i]), imag(XEpsilon[i]));
		write(epsOutput, out);
	end;
end;